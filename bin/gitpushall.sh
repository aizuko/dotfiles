#!/usr/bin/env bash
# Pushes this branch to all remotes
if ! git branch >/dev/null; then
  exit 1
fi

declare -r branch="$(git branch | grep '*' | cut -c 3-)"

git remote -v |
  grep '\(push\)' |
  awk -F' ' -vbranch="$branch" '{ system("(git push " $1 " " branch ") &") }'
