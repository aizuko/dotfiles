#!/usr/bin/env bash
# Gets the time of the last archlinux update.

declare -i last_time="$(
  rg -B1 'pacman -S(u|[yu]{2})$' "${HOME}/.bash_eternal_history" \
    | tac \
    | awk '$0 ~ /^#/ { print substr($0, 2); exit }'
)"
declare -i now="$(date '+%s')"

declare last_time_fmt="$(date -d "@${last_time}" "+%Y-%m-%d %H:%M:%S")"
declare days_ago="$(( (now-last_time)/(3600*24) ))"

printf 'Last updated at %s | %d days ago\n' "$last_time_fmt" "$days_ago"
