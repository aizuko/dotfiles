-- Sync vim configs to neovim
vim.opt.runtimepath:prepend("~/.vim")
vim.opt.runtimepath:append("~/.vim/after")
-- vim.opt.packpath = vim.opt.runtimepath
vim.cmd("source ~/.vim/init_scripts/init.vim")

-- Astolfo-inspired welcome prompt
print("UwU")

vim.opt.guicursor = "n-v-c:block-Cursor/lCursor-blinkon0,i-ci:ver25-Cursor/lCursor,r-cr:hor20-Cursor/lCursor"
vim.opt.inccommand = "nosplit"

if not vim.fn.has('python3') then
    print('Warning: No python 3 support. UltiSnips won\'t be available')
    print('Run: $ python3 -m pip install --user --upgrade pynvim')
end

-- Only load this in Neovim
if vim.fn.has('nvim') == 1 then
  local lspconfig = require('lspconfig')

  lspconfig.tsserver.setup{
    autostart = false,
  }

  lspconfig.cssls.setup{
    autostart = false,
  }

  lspconfig.html.setup{
    autostart = false,
  }

  -- React Native
  lspconfig.flow.setup{
    autostart = false,
  }

  lspconfig.clangd.setup{
    autostart = false,
  }

  -- Function to start LSP
  function LspUp()
    vim.cmd[[LspStart]]
  end

  function LspDown()
    vim.cmd[[LspStop]]
    vim.cmd("echo 'lsp is down >~<'")
  end

  -- Command to call ComposeUp
  vim.cmd[[command! LspUp lua LspUp()]]
  vim.cmd[[command! LspDown lua LspDown()]]
end

vim.api.nvim_create_autocmd('LspAttach', {
  group = vim.api.nvim_create_augroup('UserLspConfig', {}),
  callback = function(ev)
    -- Enable completion triggered by <c-x><c-o>
    vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

    -- Buffer local mappings.
    -- See `:help vim.lsp.*` for documentation on any of the below functions
    local opts = { buffer = ev.buf }
    vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
    vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
    vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
    vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
    vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)
    -- Add more mappings as needed
    local client = vim.lsp.get_client_by_id(ev.data.client_id)
    client.server_capabilities.semanticTokensProvider = nil
    vim.cmd("echo 'lsp is UP >ᴗ<'")
  end,
})

vim.api.nvim_create_autocmd("BufRead", {
  callback = function()
    vim.treesitter.stop()
  end,
})


vim.g.clipboard = {
  name = 'OSC 52',
  copy = {
    ['+'] = require('vim.ui.clipboard.osc52').copy('+'),
    ['*'] = require('vim.ui.clipboard.osc52').copy('*'),
  },
  paste = {
    ['+'] = require('vim.ui.clipboard.osc52').paste('+'),
    ['*'] = require('vim.ui.clipboard.osc52').paste('*'),
  },
}
