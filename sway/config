# Default config for sway
#
# Copy this to ~/.config/sway/config and edit it to your liking.
#
# Read `man 5 sway` for a complete reference.

#╔─────────────────────────────────────────────────────────────────────────────╗
#│ Sεττiηg ναriαblεs                                                           |
#╚─────────────────────────────────────────────────────────────────────────────╝
#  Linux      MacOS      Wayland    identifier    code
#   Alt        Opt      Alt_{L,R}      Mod1       00000008
# Logo key   Command   Super_{L,R}     Mod4       00000040
# Control     Ctrl    Control_{L,R}   Control     00000004
#  Shift      Shift    Shift_{L,R}
#    Fn        Fn        XF86Fn
#
# Arrow keys are Left|Down|Up|Right
#
set $mod1 Mod4
set $mod2 Mod4+Shift

# Pane movement keys - vim
set $pane_left h
set $pane_down j
set $pane_up k
set $pane_right l

# Symmetric keys - mpv
set $left_outer q
set $left_inner w
set $right_inner e
set $right_outer r

# Space movement keys - gaming
set $space_up w
set $space_left a
set $space_down s
set $space_right d

# Resize keys - vim windows
set $size_up   comma
set $size_down period

set $term alacritty
set $menu fuzzel -fmonospace:size=14 -l10 -w40 \
        -b222222e0 -t8ec07cff -mffaf00ff -s222222ff -Sebdbb2ff -Cffaf00ff


# Sound effects and additional features
set $volume_command ~/.config/sway/pulse_audio_volume.sh
set $volume_change_sound  ~/.config/sway/volume_change_sound.mp3
set $screenshot_sound ffplay -nodisp -autoexit -v error ~/.config/sway/screenshot_sound.mp3

# Duplicate screenshot path, for quick uploads
set $screenshot_tmp /dev/shm/screenshot_shm.png
set $screenshot_script ~/.configs_pointer/bin/screenshot_wayland.py

set $toggle_gaps ~/.configs_pointer/sway/toggle_gaps.py

#╔─────────────────────────────────────────────────────────────────────────────╗
#│ Pαηεs αηd cδηταiηεrs                                                        |
#╚─────────────────────────────────────────────────────────────────────────────╝
  # Move focus
bindsym $mod1+$pane_left  focus left
bindsym $mod1+$pane_down  focus down
bindsym $mod1+$pane_up    focus up
bindsym $mod1+$pane_right focus right

  # Move focused window
bindsym $mod2+$pane_left  move left
bindsym $mod2+$pane_down  move down
bindsym $mod2+$pane_up    move up
bindsym $mod2+$pane_right move right

  # Layout modifiers
bindsym $mod2+f floating toggle
bindsym $mod2+s layout stacking
bindsym $mod2+t layout tabbed
bindsym $mod2+r layout toggle

  # Binary split container
bindsym $mod1+b split toggle
bindsym $mod1+v split none
bindsym $mod1+m fullscreen

  # Switch container layout   Stacking|Tabbed|Rotate-bsp
bindsym $mod1+s layout stacking
bindsym $mod1+t layout tabbed
bindsym $mod1+r layout toggle split

  # Traverse window tree
bindsym $mod1+u focus parent
bindsym $mod1+d focus child

  # Alt-tab cycles windows, like on proprietary systems
bindsym $mod1+tab exec ~/.config/sway/cycle_windows.py next
bindsym $mod2+tab exec ~/.config/sway/cycle_windows.py previous

  # Swap focus between the tiling area and the floating area
#bindsym $mod+space focus mode_toggle

  # Important rust jerk
bindsym $mod1+ctrl+r exec wtype "asExpectedOfRust"

  # Automatic upload image to discord
bindsym $mod1+ctrl+u exec sudo -u emiliko ~/.configs_pointer/bin/auto_image_upload_discord.sh

  # Paste by typing
bindsym Mod4+Ctrl+v exec ~/.configs_pointer/bin/wpastetype.py

# rEsize containers ====
bindsym $mod1+e mode "resize"

mode "resize" {
    # Horizontal, shift for fine adjust
    bindsym $size_up         resize grow   width  16px
    bindsym $size_down       resize shrink width  16px
    bindsym $size_up+Shift   resize grow   width  4px
    bindsym $size_down+Shift resize shrink width  4px

    # Vertical, shift for fine adjust
    bindsym $mod1+$size_down       resize shrink height 16px
    bindsym $mod1+$size_up         resize grow   height 16px
    bindsym $mod1+$size_down+Shift resize shrink height 4px
    bindsym $mod1+$size_up+Shift   resize grow   height 4px

    # Escaping
    bindsym $mod1+f mode "workspace_tuning"

    bindsym Shift+end mode "default"
    bindsym Ctrl+k mode "default"
    bindsym Ctrl+bracketleft mode "default"
    bindsym Escape mode "default"
}

# Gaps ====
bindsym $mod2+bracketright gaps inner current minus 3
bindsym $mod2+bracketleft  gaps inner current plus 3
#bindsym $mod2+equal gaps toggle
    # Standard outer gapping for 27" screen
bindsym $mod2+c exec $toggle_gaps toggle 100 200
    # Same as above, shifted left slightly
bindsym $mod2+g gaps vertical current set 100, \
                gaps left current set 150, \
                gaps right current set 250

bindsym $mod1+g mode "gapping"
mode "gapping" {
    # General resize
    bindsym $mod2+bracketright gaps inner current minus 3
    bindsym $mod2+bracketleft  gaps inner current plus 3

    # Horizontal
    bindsym       q gaps left  current minus 2
    bindsym       w gaps left  current plus  2
    bindsym       e gaps right current plus  2
    bindsym       r gaps right current minus 2

    # Vertical
    bindsym $mod1+q gaps top    current minus 2
    bindsym $mod1+w gaps top    current plus  2
    bindsym $mod1+e gaps bottom current plus  2
    bindsym $mod1+r gaps bottom current minus 2

    # Escaping
    bindsym Shift+end mode "default"
    bindsym Ctrl+k mode "default"
    bindsym Ctrl+bracketleft mode "default"
    bindsym Escape mode "default"
}

#╔─────────────────────────────────────────────────────────────────────────────╗
#│ Wδrksραcεs                                                                  |
#╚─────────────────────────────────────────────────────────────────────────────╝
bindsym $mod1+bracketleft workspace prev
bindsym $mod1+bracketright workspace next

bindsym $mod2+a workspace prev
bindsym $mod2+d workspace next

bindsym $mod1+1 workspace number 1
bindsym $mod1+2 workspace number 2
bindsym $mod1+3 workspace number 3
bindsym $mod1+4 workspace number 4
bindsym $mod1+5 workspace number 5
bindsym $mod1+6 workspace number 6
bindsym $mod1+7 workspace number 7
bindsym $mod1+8 workspace number 8
bindsym $mod1+9 workspace number 9

# Advanced workspace tuning ====
bindsym $mod1+f mode "workspace_tuning"

mode "workspace_tuning" {

      # Move focused container to workspace
    bindsym 1 move container to workspace number 1
    bindsym 2 move container to workspace number 2
    bindsym 3 move container to workspace number 3
    bindsym 4 move container to workspace number 4
    bindsym 5 move container to workspace number 5
    bindsym 6 move container to workspace number 6
    bindsym 7 move container to workspace number 7
    bindsym 8 move container to workspace number 8
    bindsym 9 move container to workspace number 9

    # Swap containers
    bindsym $mod1+$pane_left mark --add "_swap", focus left, swap container with mark "_swap", focus left, unmark "_swap"
    bindsym $mod1+$pane_down mark --add "_swap", focus down, swap container with mark "_swap", focus down, unmark "_swap"
    bindsym $mod1+$pane_up mark --add "_swap", focus up, swap container with mark "_swap", focus up, unmark "_swap"
    bindsym $mod1+$pane_right mark --add "_swap", focus right, swap container with mark "_swap", focus right, unmark "_swap"

    # Stack, Tab, Rotate
    bindsym s layout stacking
    bindsym t layout tabbed
    bindsym r layout toggle split

    bindsym f fullscreen
    bindsym Shift+f fullscreen

    # Escaping
    bindsym $mod1+e mode "resize"

    bindsym Shift+end mode "default"
    bindsym Ctrl+k mode "default"
    bindsym Ctrl+bracketleft mode "default"
    bindsym Escape mode "default"
}

#╔─────────────────────────────────────────────────────────────────────────────╗
#│ Iηρμτ αηd δμτρμτ αdjμsτmεητs                                                |
#╚─────────────────────────────────────────────────────────────────────────────╝
# Trackpad more like MacOS
input 1452:634:bcm5974 {
    tap enabled
    natural_scroll enabled
    accel_profile adaptive
    pointer_accel 0.36
}

output * background ~/.config/sway/secondary_wallpaper.png fill
output eDP-1 background ~/.config/sway/default_wallpaper.png fill
output DP-1 background ~/.config/sway/default_wallpaper.png fill
output DP-2 background ~/.config/sway/default_wallpaper.png fill
output DP-3 background ~/.config/sway/default_wallpaper.png fill

output DP-1 scale 2
output DP-2 scale 2
output DP-3 scale 2

default_border pixel 2
  # Adjust gamma. More important for external displays. Laptops are usually fine
exec wlsunset -t 6499 -T 6500 -g 0.9

# Volume controls
bindsym --locked XF86AudioRaiseVolume exec $volume_command 4 $volume_change_sound
bindsym --locked XF86AudioLowerVolume exec $volume_command -4 $volume_change_sound
bindsym --locked shift+XF86AudioRaiseVolume exec $volume_command 1 $volume_change_sound
bindsym --locked shift+XF86AudioLowerVolume exec $volume_command -1 $volume_change_sound
bindsym --locked XF86AudioMute exec ~/.config/sway/toggle_mute.sh

# External brightness control
  # For laptops
bindsym XF86MonBrightnessUp   exec light -A 1
bindsym XF86MonBrightnessDown exec light -U 1
  # For external monitors
bindsym F4 exec ddcutil setvcp 10 - 4
bindsym F5 exec ddcutil setvcp 10 + 4
bindsym Shift+F4 exec ddcutil setvcp 10 - 1
bindsym Shift+F5 exec ddcutil setvcp 10 + 1

#╔─────────────────────────────────────────────────────────────────────────────╗
#│ Hδτkεy dαεmδη                                                               |
#╚─────────────────────────────────────────────────────────────────────────────╝
  # Terminal
bindsym $mod2+Return exec $term
  # Chromium
bindsym $mod2+n exec chromium
  # Firefox
bindsym $mod2+p exec MOZ_ENABLE_WAYLAND=1 firefox --private-window
  # App launcher, like spotlight
bindsym $mod1+space exec $menu
  # Reload sway config
bindsym $mod1+ctrl+s reload
  # Close window
bindsym $mod2+q kill
  # Dragging windows
floating_modifier $mod1 normal
  # Screenlock
bindsym $mod2+i exec ~/.config/sway/brightness_lock.sh
  # Screen kill (turns off display output, but keeps virtual display active)
bindsym --locked $mod2+u output DP-1 power toggle
  # Scratchpad
bindsym $mod1+Shift+minus move scratchpad
bindsym $mod1+minus scratchpad show
  # Warpd - Keyboard-driven mouse
bindsym Mod4+Control+j exec warpd --hint
bindsym Mod4+Control+n exec warpd --normal
  # Xremap
exec sudo ~/.configs_pointer/bin/switch_keyboard.sh pc
  # IME Module
bindsym $mod1+i exec ~/.config/sway/toggle_fcitx.sh
  # Mako notifications
exec mako
  # Exit sway (hit 3 times repeatedly to force exit)
bindsym $mod2+Escape exec ~/.config/sway/sway_exit.sh

# Screenshots ====
bindsym $mod2+6 mode "screenshots"
bindsym $mod2+5 mode "screenshots"; exec $screenshot_script take select -c $screenshot_tmp && $screenshot_sound
bindsym $mod2+4 mode "screenshots"; exec $screenshot_script take exact "$(~/.config/sway/window_dimensions.py)" && \
      $screenshot_script edit -c -q 100 -d "$(colo.sh -t)" -e png --overwrite $screenshot_tmp && $screenshot_sound

mode "screenshots" {
    # Screenshooting in processing "steps"
    # 1. Get screenshot
    bindsym a exec $screenshot_script take select -c $screenshot_tmp && $screenshot_sound
    bindsym f exec $screenshot_script take full -c $screenshot_tmp && $screenshot_sound
    bindsym m mode "default"; exec $screenshot_script markup -c $screenshot_tmp && $screenshot_sound
    bindsym g mode "default"; exec gimp $($screenshot_script markup --show-latest)

    # 2. Downsize the screenshot, since 4k is too big
    bindsym 1 exec $screenshot_script edit -c -e png -q '40%' -r '50%' $screenshot_tmp && $screenshot_sound
    bindsym 2 exec $screenshot_script edit -c -e png -q '40%' -r '60%' $screenshot_tmp && $screenshot_sound
    bindsym 3 exec $screenshot_script edit -c -e png -q '40%' -r '70%' $screenshot_tmp && $screenshot_sound
    bindsym 4 exec $screenshot_script edit -c -e png -q '40%' -r '80%' $screenshot_tmp && $screenshot_sound
    bindsym 5 exec $screenshot_script edit -c -e png -q '80%' -r '50%' $screenshot_tmp && $screenshot_sound
    bindsym 6 exec $screenshot_script edit -c -e png -q '80%' -r '60%' $screenshot_tmp && $screenshot_sound
    bindsym 7 exec $screenshot_script edit -c -e png -q '80%' -r '70%' $screenshot_tmp && $screenshot_sound
    bindsym 8 exec $screenshot_script edit -c -e png -q '80%' -r '80%' $screenshot_tmp && $screenshot_sound
    bindsym 9 exec $screenshot_script edit -c -e png -q '80%' -r '90%' $screenshot_tmp && $screenshot_sound
    bindsym 0 exec $screenshot_script edit -c -e png -q '80%' -r '100%' $screenshot_tmp && $screenshot_sound

    # OR compress image
    bindsym space mode "shrink_screenshots"

    # 3. Copy the smaller image back to clipboard and return to default
    bindsym return mode "default"
    bindsym Shift+end mode "default"
    bindsym Ctrl+k mode "default"
    bindsym Ctrl+bracketleft mode "default"
    bindsym Escape mode "default"
}

mode "shrink_screenshots" {
    # 2. Compress the screenshot, hard
    bindsym 1 exec $screenshot_script edit -c -e webp -q '40%' -r '50%' $screenshot_tmp && $screenshot_sound
    bindsym 2 exec $screenshot_script edit -c -e webp -q '40%' -r '60%' $screenshot_tmp && $screenshot_sound
    bindsym 3 exec $screenshot_script edit -c -e webp -q '40%' -r '70%' $screenshot_tmp && $screenshot_sound
    bindsym 4 exec $screenshot_script edit -c -e webp -q '40%' -r '80%' $screenshot_tmp && $screenshot_sound
    bindsym 5 exec $screenshot_script edit -c -e webp -q '80%' -r '50%' $screenshot_tmp && $screenshot_sound
    bindsym 6 exec $screenshot_script edit -c -e webp -q '80%' -r '60%' $screenshot_tmp && $screenshot_sound
    bindsym 7 exec $screenshot_script edit -c -e webp -q '80%' -r '70%' $screenshot_tmp && $screenshot_sound
    bindsym 8 exec $screenshot_script edit -c -e webp -q '80%' -r '80%' $screenshot_tmp && $screenshot_sound
    bindsym 9 exec $screenshot_script edit -c -e webp -q '80%' -r '90%' $screenshot_tmp && $screenshot_sound
    bindsym 0 exec $screenshot_script edit -c -e webp -q '80%' -r '100%' $screenshot_tmp && $screenshot_sound

    bindsym space mode "screenshots"

    # 3. Back out
    bindsym return mode "default"
    bindsym Shift+end mode "default"
    bindsym Ctrl+k mode "default"
    bindsym Ctrl+bracketleft mode "default"
    bindsym Escape mode "default"
}

# Global mpv ====
bindsym $mod2+m mode "mpv_global"

mode "mpv_global" {
    bindsym j exec ~/.config/sway/mpv_keys.sh 'j'
    bindsym k exec ~/.config/sway/mpv_keys.sh 'k'
    bindsym l exec ~/.config/sway/mpv_keys.sh 'l'
    bindsym Left exec ~/.config/sway/mpv_keys.sh 'LEFT'
    bindsym Right exec ~/.config/sway/mpv_keys.sh 'RIGHT'
    bindsym space exec ~/.config/sway/mpv_keys.sh 'SPACE'
    bindsym bracketleft exec ~/.config/sway/mpv_keys.sh '['
    bindsym bracketright exec ~/.config/sway/mpv_keys.sh ']'
    bindsym minus exec ~/.config/sway/mpv_keys.sh '-'
    bindsym equal exec ~/.config/sway/mpv_keys.sh '='
    bindsym m exec ~/.config/sway/mpv_keys.sh 'm'
    bindsym A exec ~/.config/sway/mpv_keys.sh 'A'
    bindsym 0 exec ~/.config/sway/mpv_keys.sh '0'
    bindsym 1 exec ~/.config/sway/mpv_keys.sh '1'
    bindsym 2 exec ~/.config/sway/mpv_keys.sh '2'
    bindsym 3 exec ~/.config/sway/mpv_keys.sh '3'
    bindsym 4 exec ~/.config/sway/mpv_keys.sh '4'
    bindsym 5 exec ~/.config/sway/mpv_keys.sh '5'
    bindsym 6 exec ~/.config/sway/mpv_keys.sh '6'
    bindsym 7 exec ~/.config/sway/mpv_keys.sh '7'
    bindsym 8 exec ~/.config/sway/mpv_keys.sh '8'
    bindsym 9 exec ~/.config/sway/mpv_keys.sh '9'

    bindsym return mode "default"
    bindsym Ctrl+k mode "default"
    bindsym Shift+End mode "default"
    bindsym Escape mode "default"
}

#╔─────────────────────────────────────────────────────────────────────────────╗
#│ Sτylεs                                                                      |
#╚─────────────────────────────────────────────────────────────────────────────╝
bar {
    position bottom

    binding_mode_indicator yes

      # Vertical horizontal
    gaps 0 10

    mode dock

    status_command while ~/.config/sway/swaybar_status.sh; do sleep 60; done

    colors {
          # Foreground color
        statusline #e7d6ad
        background #22222200

          # border background text
        focused_workspace #F4B36Eff #F4B36Eff #000000
        inactive_workspace #00000000 #444444 #e7d6ad
        urgent_workspace #fadb2f #fadb2f #000000

        #binding_mode #8ec07c #8ec07c #000000
        #binding_mode #8ec07c #d3869b #000000
        binding_mode #fadb2f #fadb2f #000000
    }
}

# Colors
client.focused #F4A66E #F4A66E #000000 #F4A66E #F4A66E
#client.focused #FFFFFF #FFFFFF #000000 #ffffff #ffffff

#╔─────────────────────────────────────────────────────────────────────────────╗
#│ Oτhεr                                                                       |
#╚─────────────────────────────────────────────────────────────────────────────╝
# Sleep settings for laptops  TODO: systemctl suspend?
set $laptop 'eDP-1'
set $sleep sleep 3 && swaylock

bindswitch --reload --locked lid:on exec \
    swaymsg output $laptop power off && swaylock
bindswitch --reload --locked lid:off exec \
    swaymsg output $laptop power on

exec swayidle -w timeout 1200 '~/.config/sway/suspend_system.sh'

#bindswitch --reload --locked lid:on exec swaymsg output $laptop_screen disable

#bindswitch --reload --locked lid:on exec '[ $(swaymsg -t get_outputs | grep name | wc -l) == 1 ] && ($sleep) || ($notify "Clamshell mode" "Laptop screen off" && swaymsg output $laptop_screen disable)'
#
#bindswitch --reload --locked lid:off output $laptop_screen enable

include /etc/sway/config.d/*

#
# Example configuration:
#
#   output HDMI-A-1 resolution 1920x1080 position 1920,0
#
# You can get the names of your outputs by running: swaymsg -t get_outputs

#
#exec swayidle -w \
#    timeout 101 'swaylock' \
#    timeout 314 'swaymsg "output * power off"' \
#        resume  'swaymsg "output * power on"'  \
#    timeout 626 'systemctl suspend ' \
#    before-sleep 'swaylock'

### Idle configuration
#
# Example configuration:
#
# exec swayidle -w \
#          timeout 300 'swaylock -f -c 000000' \
#          timeout 600 'swaymsg "output * power off"' resume 'swaymsg "output * power on"' \
#          before-sleep 'swaylock -f -c 000000'
#
# This will lock your screen after 300 seconds of inactivity, then turn off
# your displays after another 300 seconds, and turn your screens back on when
# resumed. It will also lock your screen before your computer goes to sleep.
