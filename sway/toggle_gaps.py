#!/usr/bin/env python3
# Toggles between none and some gaps
#
# EXAMPLES:
#   ./toggle_gaps.py reset  # Force reset current workspace
#   ./toggle_gaps.py toggle <vert> <horiz>  # Toggle between reset and gaps
#   ./toggle_gaps.py toggle <vertical> <horizontal>
#   ./toggle_gaps.py all <vertical> <horizontal>
import sys
import json
import subprocess


def get_workspaces():
    swaymsg = subprocess.Popen(
        ["swaymsg", "-t", "get_workspaces", "--raw"],
        stdout=subprocess.PIPE,
        stderr=subprocess.DEVNULL,
    )

    stdout, _ = swaymsg.communicate(2)
    return json.loads(stdout)


def get_current_workspace():
    workspaces = get_workspaces()
    return next(filter(lambda x: x["focused"], workspaces))


def has_gaps(workspace):
    rect = workspace["rect"]
    return rect["x"] > 0 or rect["y"] > 0


def swaymsg_gaps(v, h, is_all):
    swaymsg = subprocess.Popen(
        [
            "swaymsg",
            "gaps",
            "vertical",
            "all" if is_all else "current",
            "set",
            str(v),
            ",",
            "gaps",
            "horizontal",
            "all" if is_all else "current",
            "set",
            str(h)
        ]
    )

    swaymsg.communicate(1)


def toggle_workspace(ws, v, h):
    if has_gaps(ws):
        swaymsg_gaps(0, 0, False)
    else:
        swaymsg_gaps(v, h, False)


def print_help():
    print(
        """\
Toggles between none and some gaps

EXAMPLES:
./toggle_gaps.py reset  # Force reset current workspace
./toggle_gaps.py toggle <vert> <horiz>  # Toggle between reset and gaps
./toggle_gaps.py toggle <vertical> <horizontal>
./toggle_gaps.py all <vertical> <horizontal>"""
    )


# ╔───────────────────────────────────────────────────────────────────────────╗
# │ Mαiη Prδgrαm                                                              |
# ╚───────────────────────────────────────────────────────────────────────────╝

if len(sys.argv) < 2:
    print_help()
    exit(1)
elif sys.argv[1] == "reset":
    swaymsg_gaps(0, 0, False)
elif sys.argv[1] == "toggle":
    v, h = sys.argv[2:4]
    ws = get_current_workspace()
    toggle_workspace(ws, v, h)
elif sys.argv[1] == "all":
    v, h = sys.argv[2:4]
    swaymsg_gaps(v, h, True)
else:
    print_help()
    exit(1)
